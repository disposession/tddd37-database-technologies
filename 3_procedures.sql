DROP PROCEDURE IF EXISTS addYear;
DROP PROCEDURE IF EXISTS addDay;
DROP PROCEDURE IF EXISTS addDestination;
DROP PROCEDURE IF EXISTS addRoute;
DROP PROCEDURE IF EXISTS addFlight;

DELIMITER //

-- CALL addYear(1991, 1.5);
CREATE PROCEDURE addYear (
    IN year SMALLINT(20),
    IN factor DOUBLE(5, 2)
)
BEGIN
  INSERT INTO year_profit_factor (year, profit_factor) VALUES (year, factor);
END //

-- CALL addDay(1991, 'mon', 1.3);
CREATE PROCEDURE addDay (
    IN year SMALLINT(20),
    IN day VARCHAR(10),
    IN factor DOUBLE(5, 2)
)
BEGIN
    -- What should we do to year??
    INSERT INTO weekdays (day, factor) VALUES (day, factor);
END //

-- CALL addDestination('OPO', "Sa carneiro", "Portugal");
CREATE PROCEDURE addDestination (
    IN airp_code VARCHAR(3),
    IN city_name VARCHAR(30),
    IN airp_country VARCHAR(30)
)
BEGIN
    INSERT INTO airport (code, city, country) VALUES (airp_code, city_name, airp_country);
END //

-- CALL addRoute('OPO', 'LIS', 1991, 70);
CREATE PROCEDURE addRoute (
    IN dep_code VARCHAR(3),
    IN arr_code VARCHAR(3),
    IN year INT,
    IN route_price DOUBLE(12,2)
)
BEGIN
    INSERT INTO route (departing_airport, arrival_airport, year, route_price) VALUES (dep_code, arr_code, year, route_price);
END //

-- CALL addFlight('OPO', 'LIS', 1991, 'mon', '11:30:45');
CREATE PROCEDURE addFlight (
    IN dep_code VARCHAR(3),
    IN arr_code VARCHAR(3),
    IN input_year INT,
    IN input_day VARCHAR(10),
    IN input_time TIME
)
BEGIN
    DECLARE route_id INT;
    DECLARE week_number INT;
    DECLARE schedule_id INT;
    DECLARE airplane_model VARCHAR(15);
    SET week_number = 1;
    SET airplane_model = (SELECT model from airplane LIMIT 1);

    SET route_id = (SELECT id FROM route WHERE departing_airport=dep_code AND arrival_airport=arr_code AND year=input_year LIMIT 1);
    INSERT INTO weekly_schedule (day, time, route) VALUES (input_day, input_time, route_id);
    SET schedule_id = (SELECT id FROM weekly_schedule WHERE day=input_day AND time=input_time and route = route_id );
    
    loop_label: LOOP
	INSERT INTO flight (week_number, schedule, airplane) VALUES (week_number, schedule_id, airplane_model);
	SET week_number = week_number + 1;
	IF week_number > 52 THEN
		LEAVE loop_label;
	END IF;
	iterate loop_label;
    END LOOP loop_label;
END //

DELIMITER ;

-- CALL addYear(2015, 3.5);
-- CALL addDay(2015, 'mon', 1.3);
-- CALL addDestination('OPO', "Sa carneiro", "Portugal");
-- CALL addDestination('STR', "Stuttgart", "Germany");
-- CALL addRoute('OPO', 'STR', 2015, 150);
-- CALL addFlight('OPO', 'STR', 2015, 'mon', '11:30:45');
