DROP TABLE IF EXISTS passenger_reservation CASCADE;
DROP TABLE IF EXISTS reservation CASCADE;
DROP TABLE IF EXISTS flight CASCADE;
DROP TABLE IF EXISTS weekly_schedule CASCADE;
DROP TABLE IF EXISTS route CASCADE;
DROP TABLE IF EXISTS airport CASCADE;
DROP TABLE IF EXISTS weekdays CASCADE;
DROP TABLE IF EXISTS credit_card CASCADE;
DROP TABLE IF EXISTS customer CASCADE;
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS airplane CASCADE;
DROP TABLE IF EXISTS year_profit_factor CASCADE;

CREATE TABLE airport
	(code VARCHAR(3),
	 name VARCHAR(30),
	 city VARCHAR(30),
	 country VARCHAR(30),
	 PRIMARY KEY (code)) ENGINE=InnoDB;

CREATE TABLE route
	(id INT AUTO_INCREMENT,
	 departing_airport VARCHAR(3),
	 arrival_airport VARCHAR(3),
	 year INT UNSIGNED,
	 route_price DOUBLE(12,2),
	 PRIMARY KEY(id),
	 FOREIGN KEY (departing_airport) REFERENCES airport(code),
	 FOREIGN KEY (arrival_airport) REFERENCES airport(code)) ENGINE=InnoDB;


CREATE TABLE weekdays
	(day VARCHAR(10),
	 factor DOUBLE(5,2),
	 PRIMARY KEY(day)) ENGINE=InnoDB;

CREATE TABLE weekly_schedule
        (id INT AUTO_INCREMENT,
	 day VARCHAR(10),
         time TIME,
	 route INT REFERENCES route(id),
         PRIMARY KEY(id),
	 FOREIGN KEY (day) REFERENCES weekdays(day)) ENGINE=InnoDB;


CREATE TABLE airplane
	(model VARCHAR(15),
	 max_passengers TINYINT,
	 PRIMARY KEY(model)) ENGINE=InnoDB;

CREATE TABLE flight
	(id INT AUTO_INCREMENT,
	 week_number TINYINT,
	 schedule INT,
	 airplane VARCHAR(15) REFERENCES airplane(model),
	 PRIMARY KEY(id),
	 FOREIGN KEY (schedule) REFERENCES weekly_schedule(id)) ENGINE=InnoDB;

CREATE TABLE person
	(id INT AUTO_INCREMENT,
	 passport_number INT UNSIGNED,
	 name VARCHAR(30),
	 PRIMARY KEY(id)) ENGINE=InnoDB;

CREATE TABLE customer
	(person INT,
	 mail VARCHAR(30),
	 phone_number BIGINT,
	 PRIMARY KEY(person),
	 FOREIGN KEY (person) REFERENCES person(id)) ENGINE=InnoDB;

CREATE TABLE credit_card
        (number BIGINT UNSIGNED,
         card_holder VARCHAR(60),
	 customer INT,
         PRIMARY KEY(number),
	 FOREIGN KEY (customer) REFERENCES customer(person)) ENGINE=InnoDB;


CREATE TABLE reservation
	(number INT UNSIGNED,
         price decimal(12,2),
	 flight INT,
         made_by INT,
         contact_person INT,
	 is_booking BOOL DEFAULT false,
	 PRIMARY KEY(number),
	 FOREIGN KEY (flight) REFERENCES flight(id),
	 FOREIGN KEY (contact_person) REFERENCES customer(person),
	 FOREIGN KEY (made_by) REFERENCES customer(person)) ENGINE=InnoDB;


CREATE TABLE passenger_reservation
        (reservation_number INT UNSIGNED,
	 ticket_number INT UNSIGNED,
         person INT,
         PRIMARY KEY(reservation_number, person),
	 FOREIGN KEY(reservation_number) REFERENCES reservation(number),
	 FOREIGN KEY (person) REFERENCES person(id)) ENGINE=InnoDB;


CREATE TABLE year_profit_factor
	(year SMALLINT UNSIGNED,
	 profit_factor DOUBLE(5,2),
	 PRIMARY KEY(year)) ENGINE=InnoDB;

-- Insert some initial useful data
insert into airplane (model, max_passengers) VALUES ('CoolCruiser', 40);
