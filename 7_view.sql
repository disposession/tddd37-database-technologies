DROP VIEW IF EXISTS allFlights;

Create VIEW allFlights AS
SELECT f.week_number AS "departure_week", s.day AS "departure_day", s.time AS "departure_time", r.year AS "departure_year", a1.city AS "departure_city_name", a2.city AS "destination_city_name", calculateFreeSeats(f.id) AS "nr_of_free_seats", calculatePrice(f.id) AS "current_price_per_seat" FROM flight f, weekly_schedule s, route r, airport a1, airport a2 WHERE f.schedule = s.id AND s.route = r.id AND r.departing_airport = a1.code AND r.arrival_airport = a2.code;

