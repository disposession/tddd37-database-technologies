DROP FUNCTION IF EXISTS calculateFreeSeats;
DROP FUNCTION IF EXISTS calculatePrice;

DELIMITER //

CREATE FUNCTION calculateFreeSeats(flight_id INT)
    RETURNS INT
    READS SQL DATA
    BEGIN
        DECLARE number_of_bookings INT;
	DECLARE m_passengers INT;
	SET m_passengers = (SELECT max_passengers FROM airplane a, flight f WHERE f.id=1 AND f.airplane = a.model);
        SET number_of_bookings = (SELECT COUNT(*) FROM passenger_reservation WHERE reservation_number IN (SELECT number from reservation where flight = flight_id AND is_booking is true));
        RETURN m_passengers - number_of_bookings;
    END//

CREATE FUNCTION calculatePrice(flight_id INT)
    RETURNS DOUBLE
    READS SQL DATA
    BEGIN
	DECLARE r_price DOUBLE;	
	DECLARE weekday_factor DOUBLE;
	DECLARE booked_passengers INT;
	DECLARE p_factor DOUBLE;
        DECLARE final_result DOUBLE(12,3);
        SET booked_passengers = (SELECT COUNT(*) FROM passenger_reservation WHERE reservation_number IN (SELECT number from reservation where flight = flight_id AND is_booking is true));
	SET r_price = (SELECT route_price FROM route WHERE id=(SELECT route FROM weekly_schedule where id=(SELECT schedule FROM flight WHERE id=flight_id)));
	SET weekday_factor = (SELECT factor FROM weekdays WHERE day=(SELECT day FROM weekly_schedule where id=(SELECT schedule FROM flight WHERE id=flight_id)));
	SET p_factor = (SELECT profit_factor from year_profit_factor WHERE YEAR = (SELECT year from route where id=(SELECT route FROM weekly_schedule WHERE id=(SELECT schedule FROM flight WHERE id=flight_id))));
        SET final_result = r_price * weekday_factor * ((booked_passengers+1)/40) * p_factor;
	RETURN final_result;
    END//

DELIMITER ;

-- SELECT calculateFreeSeats(1);
-- SELECT calculatePrice(1);
