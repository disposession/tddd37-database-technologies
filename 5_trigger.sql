DROP TRIGGER IF EXISTS generate_booking_number;

DELIMITER //
CREATE TRIGGER generate_booking_number
    AFTER UPDATE ON reservation
    FOR EACH ROW BEGIN
	IF(new.is_booking IS true) THEN
        	UPDATE passenger_reservation set ticket_number = FLOOR((RAND() * (8999999998))+1000000000) WHERE reservation_number = new.number;
    	END IF;
END//
DELIMITER ;


