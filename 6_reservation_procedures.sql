DROP PROCEDURE IF EXISTS addReservation;
DROP PROCEDURE IF EXISTS addPassenger;
DROP PROCEDURE IF EXISTS addContact;
DROP PROCEDURE IF EXISTS addPayment;

DELIMITER //

-- CALL addReservation('OPO', 'LIS', 1991, 1, 'mon', '11:30:45', 5, 234346);
CREATE PROCEDURE addReservation (
    IN dep_code VARCHAR(3),
    IN arr_code VARCHAR(3),
    IN input_year INT,
    IN input_week TINYINT,
    IN input_day VARCHAR(10),
    IN input_time TIME,
    IN number_of_passengers INT,
    OUT output_number INT
)
this_proc:BEGIN
    DECLARE route_id INT;
    DECLARE schedule_id INT;
    DECLARE flight_id INT;

    SET route_id = (SELECT id FROM route WHERE departing_airport=dep_code AND arrival_airport=arr_code AND year=year LIMIT 1);
    SET schedule_id = (SELECT id FROM weekly_schedule WHERE route = route_id AND day = input_day AND time = input_time);
    SET flight_id = (SELECT id FROM flight WHERE schedule = schedule_id AND week_number = input_week);
    IF(flight_id IS NULL) THEN
    	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'There exist no flight for the given route, date and time';
   	LEAVE this_proc;
    END IF;

    IF (number_of_passengers > calculateFreeSeats(flight_id)) THEN
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'There are not enough seats available on the chosen flight';
   	LEAVE this_proc;
    END IF;
    -- What shall we do with customer?
    -- SET output_number = (SELECT FLOOR((RAND() * (8999999998))+1000000000));
    SET output_number = (SELECT FLOOR((RAND() * (99999999 - 10000000 + 1))+10000000));
    INSERT INTO reservation (number, flight, made_by) VALUES (output_number, flight_id, NULL);
END //

-- CALL addPassenger(234346, 4464645432, "Ziltoid the omnscient");
CREATE PROCEDURE addPassenger (
    IN reservation_nr INT,
    IN passport INT,
    IN passenger_name VARCHAR(30)
)
this_proc:BEGIN
    DECLARE person_id INT;
    DECLARE check_reservation_status INT;
    DECLARE check_reservation INT;
-- Let's revisit this error handling issue tomorrow
/*    DECLARE EXIT HANDLER FOR SQLSTATE '23000' 
     	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The given reservation number does not exist';*/


    -- Does this reservation exist?
    SET check_reservation = (SELECT number FROM reservation WHERE number = reservation_nr);
    IF(check_reservation IS NULL) THEN
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The given reservation number does not exist';
	LEAVE this_proc;
    END IF;

    -- Is this reservation payed for?
    SET check_reservation_status = (SELECT number FROM reservation WHERE number = reservation_nr AND is_booking is false);
    IF(check_reservation_status IS NULL) THEN
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The booking has already been payed and no futher passengers can be added';
	LEAVE this_proc;
    END IF;

    -- Create person if it doesn't exist
    INSERT INTO person (passport_number, name) SELECT * FROM (SELECT passport, passenger_name) AS tmp WHERE NOT EXISTS ( SELECT id FROM person WHERE passport_number = passport);
    SET person_id = (SELECT id FROM person WHERE passport_number = passport);

    INSERT INTO passenger_reservation (reservation_number, person) VALUES (reservation_nr, person_id);

END //

-- CALL addContact(234346, 4464645432, "ziltoid.omniscient@multiverse.sp", 912030);
CREATE PROCEDURE addContact (
    IN reservation_nr INT,
    IN passport INT,
    IN email VARCHAR(30),
    IN phone BIGINT
)
this_proc:BEGIN
    DECLARE person_id INT DEFAULT NULL;
    DECLARE check_reservation INT DEFAULT NULL;
    DECLARE check_passenger INT DEFAULT NULL;

    SET person_id = (SELECT id FROM person WHERE passport_number = passport);
    -- Does this person exist on database?
    IF(person_id IS NULL) THEN
        SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'No person data for this passport number';
	LEAVE this_proc;
    END IF;

    -- Does this reservation exist on database?
    SET check_reservation = (SELECT number FROM reservation WHERE number = reservation_nr);
    IF(check_reservation IS NULL) THEN
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The given reservation number does not exist';
	LEAVE this_proc;
    END IF;

    -- Is this customer one of the passengers?
    SET check_passenger = (SELECT person FROM passenger_reservation WHERE reservation_number = reservation_nr AND person = person_id);
    IF(check_passenger IS NULL) THEN
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The person is not a passenger of the reservation';
	LEAVE this_proc;
    END IF;

    INSERT INTO customer (person, mail, phone_number) VALUES (person_id, email, phone); 
    -- Update the reservation contact person
    UPDATE reservation SET contact_person = person_id WHERE number = reservation_nr;

END //

-- CALL addPayment(234346, "joao", 1234567890);
CREATE PROCEDURE addPayment (
    IN reservation_nr INT,
    IN cardholder VARCHAR(60),
    IN cardnumber BIGINT
)
this_proc:BEGIN
    -- Reservation has contact?
    DECLARE customer_id INT;
    DECLARE check_reservation INT DEFAULT NULL;
    DECLARE flight_id INT;
    DECLARE available_seats INT;    
    DECLARE number_of_people INT;
    DECLARE reservation_owner_id INT;
    DECLARE final_price DOUBLE;

    -- Does this reservation exist on database?
    SET check_reservation = (SELECT number FROM reservation WHERE number = reservation_nr);
    IF(check_reservation IS NULL) THEN
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The given reservation number does not exist';
	LEAVE this_proc;
    END IF;

    -- Contact person defined?
    SET customer_id = (SELECT contact_person FROM reservation WHERE number = reservation_nr);
    IF(customer_id IS NULL) THEN
    	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'The reservation has no contact yet';
   	LEAVE this_proc;
    END IF;

    -- Enough seats for people?
    SET flight_id = (SELECT flight FROM reservation WHERE number = reservation_nr);
    SET available_seats = calculateFreeSeats(flight_id);
    SET number_of_people = (SELECT COUNT(person) FROM passenger_reservation WHERE reservation_number = reservation_nr);

    IF(number_of_people > available_seats) THEN
        -- Should we delete reservation here?
	SIGNAL SQLSTATE VALUE '99999' SET MESSAGE_TEXT = 'There are not enough seats available on the flight anymore, deleting reservation';
	LEAVE this_proc;
    END IF;
    -- add payment information
    SET reservation_owner_id = (SELECT contact_person FROM reservation WHERE number = reservation_nr);

    INSERT INTO credit_card (number, card_holder, customer) VALUES (cardnumber, cardholder, reservation_owner_id);
    -- save booking total
    -- We work on the assumption that for a given reservation, the price of the ticket will be the same for each person on that reservation. So just multiply the next seat price by the number of people on this reservation
    SET final_price = number_of_people * calculatePrice(flight_id);
    UPDATE reservation SET price = final_price, is_booking = true WHERE number = reservation_nr;

END //

DELIMITER ;

